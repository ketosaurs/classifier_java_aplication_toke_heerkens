package nl.bioinf.theerkens.checker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileManager {


    public BufferedReader readFile(Path path) {
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset())) {

            return reader;

        } catch (IOException e) {
            System.out.println("error while reading the file");
            return null;
        }
    }
}

