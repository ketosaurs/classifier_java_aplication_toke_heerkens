package nl.bioinf.theerkens.controller;

import nl.bioinf.theerkens.arguments_provider.ApacheCliOptionsProvider;
import nl.bioinf.theerkens.arguments_provider.CommandLineArgsRunner;
import nl.bioinf.theerkens.weka_interactor.WekaModel;

import java.io.IOException;

class ProgramController {
    public static void main(String[] args) {
        ProgramController controller = new ProgramController();
        controller.start(args);
    }

    public void start(String[] args) {
        System.out.println("starting the program.");
        ApacheCliOptionsProvider op = CommandLineArgsRunner.runCommandline(args);

        if (op != null){
            WekaModel classifier = new WekaModel();
            try {
                classifier.classify(op);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
