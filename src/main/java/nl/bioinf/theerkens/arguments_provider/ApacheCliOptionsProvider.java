package nl.bioinf.theerkens.arguments_provider;


import nl.bioinf.theerkens.checker.FileManager;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;


public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String HELP = "help";
    private static final String DATAFILE = "datafile";
    private static final String OUTFILE = "outfile";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private BufferedReader DataFile;
    private String OutFile;


    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     *
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message.");
        Option dataFileOption = new Option("d", DATAFILE, true, "Give the datafile with the path to find it. This path needs to be from the root");
        Option outFileOption = new Option("o", OUTFILE, true, "Give the name and location for the output file. This path needs to be from the root, " +
                "if not provided the default will be the current working dir. The file name will be set to Classifications.++ ");


        options.addOption(helpOption);
        options.addOption(dataFileOption);
        options.addOption(outFileOption);


    }

    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            if (commandLine.hasOption(DATAFILE)){
                FileManager manager = new FileManager();
                Path path = Paths.get(commandLine.getOptionValue(DATAFILE));
                DataFile = manager.readFile(path);

            }
            if (commandLine.hasOption(OUTFILE)) {
                Path path = Paths.get(commandLine.getOptionValue(OUTFILE));
                OutFile= commandLine.getOptionValue(OUTFILE);
            }
            else {
                OutFile= (System.getProperty("user.dir") + File.separator + "Classifications.txt");
            }
        }
        catch (ParseException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }


    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Bird Classifier", options);
    }

    @Override
    public BufferedReader getDatafile() {
        return DataFile;
    }

    @Override
    public String getOutput() {
        return OutFile;
    }
}
