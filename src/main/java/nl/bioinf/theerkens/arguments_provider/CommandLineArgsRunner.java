package nl.bioinf.theerkens.arguments_provider;

import java.util.Arrays;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI. Class is
 * final because it is not designed for extension.
 *
 * @author michiel
 */
public final class CommandLineArgsRunner {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private CommandLineArgsRunner() {
    }

    /**
     * @param args the command line arguments
     */
    public static ApacheCliOptionsProvider runCommandline(final String[] args) {
        if (args.length == 0)
            throw new IllegalArgumentException("No options where provided, please consult the help.\n" +
                    " by using -h or --help");
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return op;
            }
            return op;
        } catch (IllegalArgumentException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();

        }
        return null;
    }
}
