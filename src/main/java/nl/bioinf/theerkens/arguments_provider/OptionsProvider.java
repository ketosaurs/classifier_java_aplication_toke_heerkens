package nl.bioinf.theerkens.arguments_provider;

import java.io.BufferedReader;

/**
 * interface that specifies which options should be provided to the tool.
 * @author michiel
 */
interface OptionsProvider {
    /**
     * serves the location of the data file.
     *
     * @return Data file location.
     */
    BufferedReader getDatafile();

    /**
     * serves the location of the outputfile
     *
     * @return c
     */
    String getOutput();
}
